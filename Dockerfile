# Building
FROM rust:latest AS BASE_IMAGE

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app

COPY . ./
RUN cargo build --release

# Runnning
FROM ubuntu:latest
WORKDIR /usr/src/app

COPY --from=BASE_IMAGE /usr/src/app/target/release/craft_test ./
RUN chmod +x craft_test

RUN apt-get update -y && apt-get install -y xvfb libxtst6 x11-xserver-utils fonts-dejavu fontconfig

CMD ["xvfb-run", "./craft_test", "-ta", "--version", "release"]
