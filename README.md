# CraftTest

A cross-platform Minecraft launcher primarily meant for running in a CI pipeline 
using xvfb, with built-in support for loading Aristois.

## Prebuilt binaries

The following (x86_64) platforms have prebuilt binaries from the CI:

* [Linux](https://gitlab.com/deftware/craft_test/builds/artifacts/master/raw/target/release/craft_test?job=linux)
* [Windows](https://gitlab.com/deftware/craft_test/builds/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/craft_test.exe?job=windows)

## Usage

Run Aristois for a specific Minecraft version with

`$ ./craft_test --aristois --version <version>`

where `<version>` can be a specific version (e.g. `1.20.4`) or type (`release`, `snapshot`)
to run all supported Minecraft versions. 

Automated testing can be performed by using the `--test` flag.

### Authentication

It is possible to use a Minecraft account with the launcher, 
by using the `--auth <client id>` argument. The
client id must be a valid Azure application client 
id authorized for use with Minecraft.

## Supported platforms

Runs natively on Windows, Linux, and macOS (amd64, aarch64).

### Wayland support

Craft test can run Minecraft 1.13.2 and above natively under Wayland, including Nvidia GPUs.
This is done using a [patched GLFW library](https://github.com/BoyOrigin/glfw-wayland/). 

## Docker

To run a test locally in a docker container, run
the following commands

```sh
$ docker build . -t craft_test:latest
$ docker run \
    --cpus 8 \
    --memory="8g" \
    -v ./libraries:/usr/src/app/libraries \
    -v ./assets:/usr/src/app/assets \
    -v ./results:/usr/src/app/results \
    --init \
    craft_test:latest
```
