use crate::util::{download, extract, Result};
use crate::minecraft::version::Version as MinecraftVersion;
use flate2::read::GzDecoder;
use log::{debug, info};
use reqwest::Client;
use serde::Deserialize;
use std::{
    fs::{remove_file, rename, File},
    path::{Path, PathBuf},
};
use tar::Archive;

#[cfg(unix)]
use std::os::unix::fs::PermissionsExt;

#[cfg(unix)]
use std::fs::Permissions;

pub enum Channel {
    General,
    EarlyAccess,
}

pub enum Version {
    Gamma,  // 17
    Alpha,  // 16
    Legacy, // 8
}

pub enum Arch {
    AMD64,
    Arm,
}

pub enum OS {
    Linux,
    Mac,
    Windows,
}

pub struct Runtime {
    pub os: OS,
    pub arch: Arch,
    pub version: i32,
    pub channel: Channel,
}

#[derive(Deserialize)]
struct ZuluPackage {
    name: String,
    download_url: String,
}

impl Channel {
    pub fn get(&self) -> &str {
        match self {
            Channel::General => "ga",
            Channel::EarlyAccess => "ea",
        }
    }
}

impl Arch {
    pub fn get(&self) -> &str {
        match self {
            Self::AMD64 => "x64",
            Self::Arm => "aarch64",
        }
    }

    pub fn current() -> Self {
        if cfg!(target_arch = "aarch64") {
            return Self::Arm;
        }
        Self::AMD64
    }
}

impl OS {
    pub fn get(&self) -> &str {
        match self {
            Self::Linux => "linux",
            Self::Mac => "mac",
            Self::Windows => "windows",
        }
    }

    pub fn current() -> Self {
        if cfg!(windows) {
            return Self::Windows;
        } else if cfg!(target_os = "macos") {
            return Self::Mac;
        }
        Self::Linux
    }
}

impl Runtime {
    pub fn from(version: &MinecraftVersion) -> Self {
        Self {
            os: OS::current(),
            arch: Arch::current(),
            version: version.java_version.major_version,
            channel: Channel::General
        }
    } 

    pub async fn download(&self, client: &Client) -> Result<()> {
        let mut extension = ".tar.gz";
        let mut list = self.get_list(client, extension).await?;
        if list.is_empty() {
            // Some older platforms only have .zip bundles available
            extension = ".zip";
            list = self.get_list(client, extension).await?;
        }
        let suffix = format!("{}_{}{extension}", self.os.get(), self.arch.get());
        let latest = list
            .iter()
            .find(|x| x.name.ends_with(&suffix))
            .expect("Cannot find runtime to download");
        info!("Downloading runtime {}", latest.download_url);
        let root = self.get_path();
        let parent = root.parent().expect("To have a parent");
        let archive = parent.join(&latest.name);
        download(client, &latest.download_url, &archive).await?;
        debug!("Extracting runtime");
        match extension {
            ".tar.gz" => extract_tar(&archive)?,
            ".zip" => extract(&archive)?,
            _ => panic!("Unknown bundle extension {extension}"),
        };
        remove_file(&archive)?;
        rename(
            parent.join(latest.name.strip_suffix(extension).expect("A valid path")),
            root,
        )?;
        Ok(())
    }

    async fn get_list(&self, client: &Client, extension: &str) -> Result<Vec<ZuluPackage>> {
        let extension = &extension[1..];
        let url = format!(
            "https://api.azul.com/metadata/v1/zulu/packages/?java_version={}&os={}&arch={}&archive_type={extension}\
            &java_package_type=jre&javafx_bundled=false&latest=true&distro_version={}&release_status={}&availability_types=CA\
            &page=1&page_size=5",
            self.version, self.os.get(), self.arch.get(), self.version, self.channel.get()
        );
        debug!("Downloading list of runtimes");
        let list = client
            .get(&url)
            .send()
            .await?
            .json::<Vec<ZuluPackage>>()
            .await?;
        Ok(list)
    }

    pub fn get_path(&self) -> PathBuf {
        PathBuf::from(format!(
            "./runtimes/{}/{}/jre-{}-{}/",
            self.arch.get(),
            self.os.get(),
            self.version,
            self.channel.get()
        ))
    }

    pub async fn get_runtime(&self, client: &Client) -> Result<PathBuf> {
        let runtime_dir = self.get_path();
        let mut java_binary = runtime_dir.clone();
        if cfg!(target_os = "macos") {
            java_binary.push(format!(
                "zulu-{}.jre/Contents/Home/bin/java",
                self.version
            ));
        } else {
            java_binary.push("bin/java");
            if cfg!(windows) {
                java_binary.set_extension("exe");
            }
        }
        if !runtime_dir.exists() {
            self.download(client).await?;
    
            #[cfg(unix)]
            std::fs::set_permissions(&java_binary, Permissions::from_mode(0o755))?;
        }
        Ok(java_binary)
    }
}

pub fn extract_tar(path: &Path) -> Result<()> {
    let tar_gz = File::open(path)?;
    let tar = GzDecoder::new(tar_gz);
    let mut archive = Archive::new(tar);

    let parent = path.parent().expect("A parent dir");
    archive.unpack(parent)?;
    Ok(())
}
