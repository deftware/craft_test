use std::borrow::Cow;
use std::collections::{HashMap, HashSet};
use std::fs::create_dir_all;
use std::path::{Path, PathBuf};
use std::process::Command;
use regex::Regex;
use std::env;

use crate::util::{download, insert_path, replace_placeholders, Result};
use dunce::canonicalize;
use log::{debug, info, warn};
use reqwest::Client;
use serde::Deserialize;

use super::assets::{AssetIndex, Assets};
use super::library::Library;
use super::rules::{is_allowed, OSRule, Rule};

#[cfg(target_os = "windows")]
use junction;

#[cfg(not(target_os = "windows"))]
use std::os::unix::fs::symlink;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Version {
    pub id: String,
    pub asset_index: AssetIndex,
    pub java_version: JavaVersion,
    pub libraries: Vec<Library>,
    pub downloads: Downloads,
    #[serde(default = "default_arguments")]
    pub arguments: Arguments,
    pub main_class: String,
    pub r#type: String,
    pub minecraft_arguments: Option<String>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JavaVersion {
    pub major_version: i32,
}

fn default_arguments() -> Arguments {
    let osx = Argument {
        rules: vec![Rule {
            action: String::from("allow"),
            os: Some(OSRule {
                name: Some(String::from("osx")),
                arch: None,
            }),
            features: None,
        }],
        value: ArgValue::String(String::from("-XstartOnFirstThread")),
    };
    Arguments {
        game: Vec::new(),
        jvm: vec![
            ArgumentEnum::Argument(osx),
            ArgumentEnum::String(String::from("-Djava.library.path=${natives_directory}")),
            ArgumentEnum::String(String::from("-cp")),
            ArgumentEnum::String(String::from("${classpath}")),
        ],
    }
}

#[derive(Deserialize)]
pub struct Download {
    pub url: String,
}

#[derive(Deserialize)]
pub struct Downloads {
    pub client: Download,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum ArgValue {
    String(String),
    Vec(Vec<String>),
}

#[derive(Deserialize)]
pub struct Argument {
    rules: Vec<Rule>,
    value: ArgValue,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum ArgumentEnum {
    String(String),
    Argument(Argument),
}

#[derive(Deserialize)]
pub struct Arguments {
    pub game: Vec<ArgumentEnum>,
    pub jvm: Vec<ArgumentEnum>,
}

impl Version {
    pub async fn download_jar(&self, client: &Client) -> Result<()> {
        let jar = self.get_jar();
        if !jar.exists() {
            let size = download(client, &self.downloads.client.url, jar).await?;
            info!("Downloaded Minecraft {} ({size} bytes)", self.id);
        }
        Ok(())
    }

    pub async fn download_assets(&self, client: &Client) -> Result<()> {
        let assets = self.asset_index.get_assets(client).await?;
        assets.download(client).await?;
        Ok(())
    }

    pub async fn download_libs(&self, client: &Client) -> Result<()> {
        for lib in &self.libraries {
            if lib.is_applicable() {
                if lib.is_native() {
                    lib.download_native(client, &self.id).await?;
                } else {
                    lib.download(client).await?;
                }
            }
        }
        Ok(())
    }

    #[cfg(target_os = "windows")]
    fn setup_assets(assets: &Path, run_dir: &Path) -> Result<()> {
        let junction = run_dir.join("assets");
        let target = std::fs::read_link(&junction);
        if target.is_ok() {
            // Exist follows the symlink, if
            // what it points to exists, we're all good
            if junction.exists() {
                return Ok(());
            }
            junction::delete(&junction)?;
        }
        if junction.exists() {
            // If the junction exists, but is a
            // normal directory instead of a symlink
            std::fs::remove_dir_all(&junction)?;
        }
        junction::create(assets, junction)?;
        Ok(())
    }

    #[cfg(not(target_os = "windows"))]
    fn setup_assets(assets: &Path, run_dir: &Path) -> Result<()> {
        let junction = run_dir.join("assets");
        if !junction.exists() {
            symlink(assets, junction)?;
        }
        Ok(())
    }

    async fn patch(&self, command: &mut Command) -> Result<()> {
        // Use custom glfw on wayland
        if env::var("XDG_SESSION_TYPE").is_ok_and(|val| val == "wayland") {
            let unsupported = Regex::new(r"^1\.(8|9|10|11|12)(\.\d+)?$")?;
            if unsupported.is_match(&self.id) {
                warn!("Running Minecraft in XWayland, this may result in graphical glitches");
                return Ok(())
            }
            info!("Using patched GLFW for Wayland");
            let lspci = Command::new("lspci").output()?;
            let devices = String::from_utf8_lossy(&lspci.stdout);
            if devices.contains("VGA compatible controller: NVIDIA") {
                debug!("Detected NVIDIA gpu");
                command.env("__GL_THREADED_OPTIMIZATIONS", "0");
            }
            let glfw = Path::new("./natives").join("libglfw.so");
            if !glfw.exists() {
                let client = Client::new();
                download(&client, "https://github.com/BoyOrigin/glfw-wayland/releases/download/2024-03-07/libglfw.so.3.5", &glfw).await?;
            }
            let path = canonicalize(glfw)?;
            command.arg(format!("-Dorg.lwjgl.glfw.libname={}", path.to_str().expect("A valid path")));   
        }
        Ok(())
    }

    pub async fn get_command(
        &self,
        runtime: &str,
        run_dir: &Path,
        auth: &HashMap<&str, &str>,
    ) -> Result<Command> {
        let assets = Assets::get_assets_dir()?;
        let natives = self.get_natives_dir()?;
        let runnable = self.get_absolute_jar()?;
        let libraries = Library::get_libraries_dir()?;
        let classpath = create_classpath(runnable, &self.libraries, |_| true)?;
        let delimiter = get_delimiter();

        // Launchwrapper sends the incorrect assets
        // dir argument, so we have to create a symlink
        if assets.exists() {
            Version::setup_assets(&assets, run_dir)?;
        }
        // Make string replacement map
        let mut map: HashMap<&str, &str> = HashMap::new();
        map.insert("assets_index_name", &self.asset_index.id);
        map.insert("version_name", &self.id);
        map.insert("launcher_name", "Minecraft Launcher");
        map.insert("launcher_version", "1.0.0");
        map.insert("version_type", &self.r#type);
        map.insert("classpath", &classpath);
        map.insert("classpath_separator", delimiter);
        map.insert("user_properties", "{}");
        // Authentication
        for (key, value) in auth {
            map.insert(key, value);
        }
        // Directories
        insert_path!(map, "game_directory", run_dir);
        insert_path!(map, "natives_directory", natives);
        insert_path!(map, "assets_root", assets);
        insert_path!(map, "game_assets", assets);
        insert_path!(map, "library_directory", libraries);
        // Create command to launch
        let gmr = Path::new("/usr/bin/gamemoderun");
        let mut command = match gmr.exists() {
            true => {
                info!("Using gamemoderun");
                let mut cmd = Command::new(gmr);
                cmd.arg(runtime);
                cmd
            },
            false => Command::new(runtime)
        };
        let jvm = parse_args(&self.arguments.jvm, &map)?;
        let game = parse_args(&self.arguments.game, &map)?;
        for arg in jvm {
            command.arg(arg.as_ref());
        }
        self.patch(&mut command).await?;
        command.arg(&self.main_class);
        for arg in game {
            command.arg(arg.as_ref());
        }
        command.current_dir(run_dir);
        Ok(command)
    }

    fn get_jar(&self) -> PathBuf {
        PathBuf::from(format!("./libraries/net/minecraft/{0}.jar", self.id))
    }

    pub fn get_run_dir(&self) -> Result<PathBuf> {
        let path = Path::new("./run").join(&self.id);
        if !path.exists() {
            create_dir_all(&path)?;
        }
        Ok(canonicalize(path)?)
    }

    pub fn get_natives_dir(&self) -> Result<PathBuf> {
        Ok(canonicalize(Path::new("./natives").join(&self.id))?)
    }

    pub fn get_absolute_jar(&self) -> Result<PathBuf> {
        Ok(canonicalize(self.get_jar())?)
    }

    pub fn add_library(&mut self, library: Library) {
        self.libraries.push(library);
    }

    pub fn add_jvm_arg(&mut self, value: ArgumentEnum) {
        self.arguments.jvm.insert(0, value);
    }

    pub fn add_game_arg(&mut self, value: ArgumentEnum) {
        self.arguments.game.push(value);
    }

    pub fn clear_game_args(&mut self) {
        self.arguments.game.clear();
    }

    pub fn set_main_class(&mut self, class: String) {
        self.main_class = class;
    }
}

pub fn create_classpath<F: Fn(&String) -> bool>(
    runnable: PathBuf,
    libs: &[Library],
    predicate: F,
) -> Result<String> {
    let mut list = Vec::new();
    let mut push_path = |path: PathBuf| {
        let str = path.into_os_string().into_string().expect("A valid string");
        list.push(str);
    };
    push_path(runnable);
    let mut added_libs = HashSet::new();
    for lib in libs.iter().rev() {
        if let Some(skip) = lib.skip_classpath {
            if skip {
                continue;
            }
        }
        if lib.is_applicable() && !lib.is_native() && predicate(&lib.name) {
            let parts = lib.name.split(":").collect::<Vec<&str>>();
            let id = [parts[0], parts[1]].join(":");
            if parts[1] != "asm" || added_libs.insert(id) {
                push_path(lib.get_absolute_path()?)
            }
        }
    }
    let delimiter = get_delimiter();
    let result = list.join(delimiter);
    Ok(result)
}

pub fn get_delimiter() -> &'static str {
    if cfg!(target_os = "windows") {
        ";"
    } else {
        ":"
    }
}

fn parse_args<'a>(
    args: &'a [ArgumentEnum],
    map: &HashMap<&str, &str>,
) -> Result<Vec<Cow<'a, str>>> {
    let mut list = Vec::new();
    let mut push_arg = |arg: &'a str| {
        let tmp = replace_placeholders(arg, map);
        list.push(tmp);
    };
    for arg in args {
        match arg {
            ArgumentEnum::String(x) => push_arg(x),
            ArgumentEnum::Argument(x) => {
                if !is_allowed(&x.rules) {
                    continue;
                }
                match &x.value {
                    ArgValue::String(y) => push_arg(y),
                    ArgValue::Vec(y) => {
                        for val in y {
                            push_arg(val);
                        }
                    }
                }
            }
        }
    }
    Ok(list)
}
