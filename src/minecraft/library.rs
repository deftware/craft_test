use super::rules::{get_current_os, is_allowed, RuleArray};
use crate::util::{download, extract, file_checksum, Result};
use dunce::canonicalize;
use log::info;
use reqwest::Client;
use serde::Deserialize;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

#[derive(Deserialize)]
pub struct Artifact {
    pub sha1: Option<String>,
    pub path: String,
    pub url: String,
}

#[derive(Deserialize)]
pub struct Downloads {
    pub artifact: Option<Artifact>,
    pub classifiers: Option<HashMap<String, Artifact>>,
}

#[derive(Deserialize)]
pub struct Library {
    pub downloads: Downloads,
    pub name: String,
    pub rules: RuleArray,
    pub natives: Option<HashMap<String, String>>,
    pub skip_classpath: Option<bool>,
}

impl Library {
    pub fn is_applicable(&self) -> bool {
        match &self.rules {
            Some(rules) => is_allowed(rules),
            None => true,
        }
    }

    pub async fn download(&self, client: &Client) -> Result<()> {
        if let Some(artifact) = &self.downloads.artifact {
            let path = Path::new("./libraries").join(&artifact.path);
            if path.exists() {
                let checksum = file_checksum(&path)?;
                let Some(remote) = &artifact.sha1 else {
                    // Return if no remote checksum exists
                    return Ok(());
                };
                if &checksum == remote {
                    return Ok(());
                }
            }
            if artifact.url.is_empty() {
                panic!("{} has no download url", self.name);
            }
            let size = download(client, &artifact.url, path).await?;
            info!("Downloaded {} ({size} bytes)", self.name);
        }
        Ok(())
    }

    pub fn get_absolute_path(&self) -> Result<PathBuf> {
        if let Some(artifact) = &self.downloads.artifact {
            let path = Path::new("./libraries").join(&artifact.path);
            return Ok(canonicalize(path)?);
        }
        panic!("Cannot get absolute path from library without artifact");
    }

    pub fn get_libraries_dir() -> Result<PathBuf> {
        Ok(canonicalize("./libraries")?)
    }

    /* Native library management */

    pub async fn download_native(&self, client: &Client, id: &str) -> Result<()> {
        if self.name.contains("natives") {
            if let Some(artifact) = &self.downloads.artifact {
                self.on_native(client, artifact, id).await?;
            }
        } else if let Some(natives) = &self.natives {
            let classifier = natives
                .get(get_current_os())
                .expect("To have a valid classifier");
            let artifact = self
                .downloads
                .classifiers
                .as_ref()
                .expect("To have a list of classifiers")
                .get(classifier);
            if let Some(x) = artifact {
                self.on_native(client, x, id).await?
            }
        }
        Ok(())
    }

    async fn on_native(&self, client: &Client, artifact: &Artifact, id: &str) -> Result<()> {
        let (_, name) = artifact
            .path
            .rsplit_once('/')
            .expect("Name to have delimiters");
        let path = Path::new("./natives").join(id).join(name);
        if !path.exists() {
            let size = download(client, &artifact.url, &path).await?;
            info!("Downloaded natives for {} ({} bytes)", self.name, size);
            extract(&path)?;
        }
        Ok(())
    }

    pub fn is_native(&self) -> bool {
        self.name.contains("natives") || self.natives.is_some()
    }
}
