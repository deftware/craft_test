use serde::Deserialize;
use serde_json::Value;

pub type RuleArray = Option<Vec<Rule>>;

#[derive(Deserialize)]
pub struct Rule {
    pub action: String,
    pub os: Option<OSRule>,
    pub features: Option<Value>,
}

#[derive(Deserialize)]
pub struct OSRule {
    pub name: Option<String>,
    pub arch: Option<String>,
}

impl OSRule {
    pub fn is_current(&self) -> bool {
        let mut result = true;
        if let Some(name) = &self.name {
            result &= name == get_current_os();
        }
        if let Some(arch) = &self.arch {
            result &= arch == get_current_arch();
        }
        result
    }
}

impl Rule {
    /// If this rule should be considered
    pub fn is_applicable(&self) -> bool {
        let mut result = true;
        if let Some(os) = &self.os {
            result &= os.is_current();
        }
        result
    }

    pub fn is_allowed(&self) -> bool {
        // TODO: Implement features
        if self.features.is_some() {
            return false;
        }
        self.action == "allow"
    }
}

pub fn is_allowed(rules: &[Rule]) -> bool {
    let mut result = false;
    for rule in rules {
        if !rule.is_applicable() {
            continue;
        }
        result = rule.is_allowed();
    }
    result
}

pub fn get_current_os() -> &'static str {
    if cfg!(target_os = "windows") {
        "windows"
    } else if cfg!(target_os = "macos") {
        "osx"
    } else {
        "linux"
    }
}

pub fn get_current_arch() -> &'static str {
    if cfg!(target_arch = "aarch64") {
        "aarch64"
    } else if cfg!(target_arch = "x86") {
        "x86"
    } else {
        "x64"
    }
}
