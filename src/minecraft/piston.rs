use crate::minecraft::version::Version;
use crate::util::{read_json_url, Result};
use reqwest::Client;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Latest {
    pub release: String,
    pub snapshot: String,
}

#[derive(Deserialize)]
pub struct Meta {
    pub id: String,
    r#type: String,
    url: String,
}

#[derive(Deserialize)]
pub struct Manifest {
    pub latest: Latest,
    pub versions: Vec<Meta>,
}

impl Manifest {
    pub async fn new(client: &Client) -> Result<Self> {
        read_json_url::<Manifest>(
            client,
            "https://piston-meta.mojang.com/mc/game/version_manifest_v2.json",
        )
        .await
    }

    pub fn list(&self) -> &[Meta] {
        &self.versions
    }

    pub fn latest(&self) -> &Meta {
        let version = &self.latest.release;
        match self.find(version) {
            Some(x) => x,
            None => panic!("Cannot find version {version}"),
        }
    }

    pub fn find(&self, id: &str) -> Option<&Meta> {
        self.versions.iter().find(|&version| version.id == id)
    }
}

impl Meta {
    pub async fn create(&self, client: &Client) -> Result<Version> {
        let mut version = read_json_url::<Version>(client, &self.url).await?;
        if let Some(args) = &version.minecraft_arguments {
            for arg in args.split(' ') {
                version
                    .arguments
                    .game
                    .push(super::version::ArgumentEnum::String(arg.to_string()));
            }
        }
        Ok(version)
    }

    pub fn is_snapshot(&self) -> bool {
        self.r#type == "snapshot"
    }
}
