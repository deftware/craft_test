use crate::util::{download, read_json_file, Result};
use dunce::canonicalize;
use reqwest::Client;
use serde::Deserialize;
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

#[derive(Deserialize)]
pub struct Asset {
    hash: String,
    size: i32,
}

#[derive(Deserialize)]
pub struct Assets {
    objects: HashMap<String, Asset>,
}

#[derive(Deserialize)]
pub struct AssetIndex {
    pub id: String,
    url: String,
}

impl AssetIndex {
    pub async fn get_assets(&self, client: &Client) -> Result<Assets> {
        let path = format!("./assets/indexes/{}.json", self.id);
        let index = PathBuf::from(path);
        if !index.exists() {
            println!("Downloading assets index {}", self.id);
            download(client, &self.url, &index).await?;
        }
        read_json_file::<Assets>(&index)
    }
}

impl Assets {
    pub async fn download(&self, client: &Client) -> Result<()> {
        let assets = Path::new("./assets/objects");
        for (name, asset) in &self.objects {
            let path = format!("{}/{}", &asset.hash[0..2], asset.hash);
            let file = assets.join(&path);
            if !file.exists() {
                println!("Downloading asset {name} ({} bytes)", asset.size);
                let url = format!("https://resources.download.minecraft.net/{path}");
                download(client, url, file).await?;
            }
        }
        Ok(())
    }

    pub fn get_assets_dir() -> Result<PathBuf> {
        let raw = "./assets";
        let path = canonicalize(raw);
        if let Ok(path) = path {
            return Ok(path)
        }
        Ok(PathBuf::from(raw))
    }
}
