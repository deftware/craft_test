use lazy_static::lazy_static;
use regex::Regex;
use reqwest::Client;
use serde::{de::DeserializeOwned, Serialize};
use serde_json::{from_reader, to_string_pretty};
use sha1::{Digest, Sha1};
use std::{
    borrow::Cow,
    collections::HashMap,
    error::Error,
    fs::{create_dir_all, read_dir, write, File},
    io::{BufReader, Read},
    path::{Path, PathBuf},
};
use zip::ZipArchive;

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

macro_rules! insert_path {
    ($map: ident, $key: expr, $path: ident) => {
        let val = $path.to_str().expect("A valid path string");
        $map.insert($key, val);
    };
}

pub(crate) use insert_path;

lazy_static! {
    static ref PLACEHOLDER_RE: Regex = Regex::new(r"\$?\{(\w+)\}").unwrap();
}

pub fn replace_placeholders<'a>(string: &'a str, map: &HashMap<&str, &str>) -> Cow<'a, str> {
    let mut tmp = Cow::from(string);
    for cap in PLACEHOLDER_RE.captures_iter(string) {
        let replace = &cap[0];
        match map.get(&cap[1]) {
            Some(x) => tmp = tmp.replace(replace, x).into(),
            None => panic!("Missing replacement for \"{replace}\" in \"{string}\""),
        }
    }
    tmp
}

pub fn recursive_dir_copy(path: &Path, dest: &Path) -> Result<()> {
    if path.is_dir() {
        for entry in read_dir(path)? {
            let entry = entry?.path();
            let name = entry.file_name().unwrap().to_str().unwrap();
            let dest_entry = dest.join(name);
            if entry.is_dir() {
                recursive_dir_copy(&entry, &dest_entry)?;
            } else {
                if !dest.exists() {
                    create_dir_all(dest)?;
                }
                std::fs::copy(&entry, &dest_entry)?;
            }
        }
    }
    Ok(())
}

pub fn extract(path: &Path) -> Result<()> {
    let parent = path.parent().expect("A parent directory");
    let file = File::open(path)?;
    let mut archive = ZipArchive::new(file)?;
    for i in 0..archive.len() {
        let mut file = archive.by_index(i)?;
        let output = match file.enclosed_name() {
            Some(path) => parent.join(path),
            None => continue,
        };
        if file.name().ends_with('/') {
            create_dir_all(output)?;
        } else {
            if let Some(p) = output.parent() {
                if !p.exists() {
                    create_dir_all(p)?;
                }
            }
            let mut outfile = File::create(&output)?;
            std::io::copy(&mut file, &mut outfile)?;
        }
    }
    Ok(())
}

pub fn file_checksum(path: &Path) -> Result<String> {
    let input = File::open(path)?;
    let mut reader = BufReader::new(input);
    let digest = {
        let mut hasher = Sha1::new();
        let mut buffer = [0; 1024];
        loop {
            let count = reader.read(&mut buffer)?;
            if count == 0 {
                break;
            }
            hasher.update(&buffer[..count]);
        }
        hasher.finalize()
    };
    let checksum = format!("{:X}", digest);
    Ok(checksum.to_lowercase())
}

pub async fn read_json_with_cache<T: DeserializeOwned>(
    client: &Client,
    name: &str,
    url: &str,
) -> Result<T> {
    let local_file = PathBuf::from(format!("./assets/cache/{name}.json"));
    if !local_file.exists() {
        download(client, url, &local_file).await?;
    }
    read_json_file::<T>(&local_file)
}

pub fn read_json_file<T: DeserializeOwned>(path: &Path) -> Result<T> {
    let file = File::open(path)?;
    let buffer = BufReader::new(file);
    let value = from_reader(buffer)?;
    Ok(value)
}

pub async fn read_json_url<T: DeserializeOwned>(client: &Client, url: &str) -> Result<T> {
    let value = client.get(url).send().await?.json::<T>().await?;
    Ok(value)
}

pub async fn read_json_or_cache<T: DeserializeOwned + Serialize>(
    client: &Client,
    url: &str,
    use_cache: bool,
) -> Result<T> {
    let name = url.split('/').last().expect("A valid url");
    let path = PathBuf::from(format!("./.cache/{name}"));
    if use_cache {
        return read_json_file(&path);
    }
    assert_parent_path(&path)?;
    let val = read_json_url(client, url).await?;
    let json = to_string_pretty(&val)?;
    write(path, json)?;
    Ok(val)
}

pub async fn download<S: AsRef<str>, T: AsRef<Path>>(
    client: &Client,
    url: S,
    path: T,
) -> Result<usize> {
    assert_parent_path(path.as_ref())?;
    let response = client.get(url.as_ref()).send().await?;
    let bytes = response.bytes().await?;
    write(path, &bytes)?;
    Ok(bytes.len())
}

pub fn assert_parent_path(path: &Path) -> Result<()> {
    let parent = path.parent().expect("Path to have a parent");
    if !parent.exists() {
        create_dir_all(parent)?;
    }
    Ok(())
}

pub fn assert_path(path: &Path) -> Result<()> {
    if !path.exists() {
        create_dir_all(path)?;
    }
    Ok(())
}
