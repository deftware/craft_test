use super::util::{read_json_file, Result};
use log::{debug, error, info};
use minecraft_msa_auth::{MinecraftAuthenticationResponse, MinecraftAuthorizationFlow};
use oauth2::basic::BasicClient;
use oauth2::devicecode::StandardDeviceAuthorizationResponse;
use oauth2::reqwest::async_http_client;
use oauth2::{AuthUrl, ClientId, DeviceAuthorizationUrl, Scope, TokenResponse, TokenUrl};
use reqwest::{Client, StatusCode};
use serde::{Deserialize, Serialize};
use serde_json::to_string_pretty;
use std::error::Error;
use std::fmt;
use std::path::Path;
use std::time::UNIX_EPOCH;
use std::{
    collections::HashMap,
    fs::{metadata, write},
    time::{Duration, SystemTime},
};

type Token =
    oauth2::StandardTokenResponse<oauth2::EmptyExtraTokenFields, oauth2::basic::BasicTokenType>;

pub struct AuthFlow {
    client: BasicClient,
}

#[derive(Deserialize, Serialize)]
pub struct Profile {
    pub id: String,
    pub name: String,
}

#[derive(Deserialize)]
pub struct AuthResponse {
    pub code: i32,
    pub expires: Timeout,
}

#[derive(Deserialize)]
pub struct Timeout {
    pub ms: i32,
    pub text: String,
}

#[derive(Deserialize)]
pub struct AuthMeta {
    pub hash: String,
}

#[derive(Debug)]
pub struct AuthError {
    details: String,
}

impl fmt::Display for AuthError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for AuthError {
    fn description(&self) -> &str {
        &self.details
    }
}

pub async fn get_access_token(client_id: &str, accounts_dir: &Path) -> Result<String> {
    let flow = AuthFlow::new(client_id)?;
    let token = flow.authorize(&accounts_dir).await?;
    let response = AuthFlow::get_minecraft_token(&token, &accounts_dir).await?;
    let token = response.access_token().to_owned();
    Ok(token.into_inner())
} 

impl AuthResponse {
    pub async fn authenticate(token: &str, profile: &Profile, client: &Client) -> Result<Self> {
        let endpoint = "https://auth.aristois.net/join";
        // Get server hash
        let meta = client
            .get(endpoint)
            .send()
            .await?
            .json::<AuthMeta>()
            .await?;
        debug!("Server hash {}", meta.hash);
        info!("Joining auth.aristois.net...");
        join_server(&meta.hash, profile, token, client).await?;
        info!("Retrieving auth token...");
        // Send request to auth.aristois.net to get token
        let mut body = HashMap::new();
        body.insert("username", &profile.name);
        let response = client.post(endpoint).json(&body).send().await?;
        let status = response.status();
        if !status.is_success() {
            let text = response.text().await?;
            error!(
                "Auth server responded with status code {} and body {text}",
                status.as_u16()
            );
            return Err(Box::from(AuthError {
                details: format!("Could not get auth token for {}", profile.name),
            }));
        }
        let auth_response = response.json::<AuthResponse>().await?;
        Ok(auth_response)
    }
}

pub async fn join_server(
    hash: &str,
    profile: &Profile,
    token: &str,
    client: &Client,
) -> Result<()> {
    let mut body = HashMap::new();
    body.insert("serverId", hash);
    body.insert("accessToken", token);
    body.insert("selectedProfile", &profile.id);
    debug!("Joining server {hash}");
    let response = client
        .post("https://sessionserver.mojang.com/session/minecraft/join")
        .json(&body)
        .send()
        .await?;
    let status = response.status();
    if status != StatusCode::NO_CONTENT {
        let text = response.text().await?;
        error!(
            "Sessionserver responded with status code {} and body {text}",
            status.as_u16()
        );
        return Err(Box::from(AuthError {
            details: format!("Unable to join server {hash}"),
        }));
    }
    Ok(())
}

impl AuthFlow {
    pub fn new(client_id: &str) -> Result<Self> {
        const DEVICE_CODE_URL: &str =
            "https://login.microsoftonline.com/consumers/oauth2/v2.0/devicecode";
        const MSA_AUTHORIZE_URL: &str =
            "https://login.microsoftonline.com/consumers/oauth2/v2.0/authorize";
        const MSA_TOKEN_URL: &str = "https://login.microsoftonline.com/consumers/oauth2/v2.0/token";
        let client = BasicClient::new(
            ClientId::new(client_id.to_string()),
            None,
            AuthUrl::new(MSA_AUTHORIZE_URL.to_string())?,
            Some(TokenUrl::new(MSA_TOKEN_URL.to_string())?),
        )
        .set_device_authorization_url(DeviceAuthorizationUrl::new(DEVICE_CODE_URL.to_string())?);
        Ok(Self { client })
    }

    pub async fn authorize(&self, accounts_dir: &Path) -> Result<Token> {
        let account = accounts_dir.join("account").with_extension("json");
        if account.exists() {
            let token = read_json_file::<Token>(&account)?;
            if let Some(expires_in) = token.expires_in() {
                if !Self::is_expired(expires_in, &account)? {
                    return Ok(token);
                }
            }
            if let Some(refresh_token) = token.refresh_token() {
                info!("Refreshing Microsoft token");
                let token = self
                    .client
                    .exchange_refresh_token(refresh_token)
                    .add_scope(Scope::new("XboxLive.signin offline_access".to_string()))
                    .request_async(async_http_client)
                    .await?;
                Self::write_to_file(&token, &account)?;
                return Ok(token);
            }
        }

        let details: StandardDeviceAuthorizationResponse = self
            .client
            .exchange_device_code()?
            .add_scope(Scope::new("XboxLive.signin offline_access".to_string()))
            .request_async(async_http_client)
            .await?;

        info!(
            "Open this URL in your browser {} and enter the code {}",
            **details.verification_uri(),
            details.user_code().secret()
        );

        let token = self
            .client
            .exchange_device_access_token(&details)
            .request_async(async_http_client, tokio::time::sleep, None)
            .await?;
        info!("Authenticated with Microsoft");

        Self::write_to_file(&token, &account)?;

        Ok(token)
    }

    pub async fn get_minecraft_token(
        token: &Token,
        accounts_dir: &Path,
    ) -> Result<MinecraftAuthenticationResponse> {
        let account = accounts_dir.join("minecraft").with_extension("json");
        if account.exists() {
            let mc_token = read_json_file::<MinecraftAuthenticationResponse>(&account)?;
            let expires_in = Duration::from_secs(mc_token.expires_in() as u64);
            if !Self::is_expired(expires_in, &account)? {
                return Ok(mc_token);
            }
        }
        info!("Acquiring Minecraft token...");
        let mc_flow = MinecraftAuthorizationFlow::new(Client::new());
        let mc_token = mc_flow
            .exchange_microsoft_token(token.access_token().secret())
            .await?;
        Self::write_to_file(&mc_token, &account)?;
        Ok(mc_token)
    }

    pub async fn get_profile(
        token: &String,
        client: &Client,
        accounts_dir: &Path,
    ) -> Result<Profile> {
        let profile_path = accounts_dir.join("profile").with_extension("json");
        if profile_path.exists() {
            let value = read_json_file::<Profile>(&profile_path)?;
            return Ok(value);
        }
        let profile = match token.as_ref() {
            "0" => Profile {
                id: "00000000-0000-0000-0000-000000000000".to_string(),
                name: "Player".to_string(),
            },
            _ => {
                debug!("Retrieving user profile");
                let profile = client
                    .get("https://api.minecraftservices.com/minecraft/profile")
                    .header("Authorization", format!("Bearer {token}"))
                    .send()
                    .await?
                    .json::<Profile>()
                    .await?;
                Self::write_to_file(&profile, &profile_path)?;
                profile
            }
        };
        Ok(profile)
    }

    fn write_to_file<T: ?Sized + Serialize>(json: &T, file: &Path) -> Result<()> {
        let json = to_string_pretty(json)?;
        write(file, json)?;
        Ok(())
    }

    fn is_expired(expires_in: Duration, file: &Path) -> Result<bool> {
        let meta = metadata(file)?;
        let modified = meta.modified()?.duration_since(UNIX_EPOCH)?;
        let expires = expires_in + modified;
        let now = SystemTime::now().duration_since(UNIX_EPOCH)?;
        Ok(now > expires)
    }
}
