use super::util::{get_maven_path, make_library};
use crate::util::{download, extract, Result};
use crate::{
    minecraft::{
        library::{Artifact, Downloads, Library},
        version::{create_classpath, ArgumentEnum, Version},
    },
    util::{insert_path, read_json_file, read_json_url, recursive_dir_copy, replace_placeholders},
};
use dunce::canonicalize;
use reqwest::Client;
use serde::Deserialize;
use std::{
    borrow::Cow,
    collections::HashMap,
    fs::{copy, create_dir_all, remove_file, File},
    io::read_to_string,
    path::Path,
    process::Command,
};
use zip::ZipArchive;

#[derive(Deserialize)]
pub struct Manifest {
    promos: HashMap<String, String>,
}

#[derive(Deserialize)]
pub struct Forge {
    version: String,
    minecraft: String,
}

#[derive(Deserialize)]
struct Processor {
    sides: Option<Vec<String>>,
    jar: String,
    classpath: Vec<String>,
    args: Vec<String>,
}

#[derive(Deserialize)]
struct Data {
    client: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct InstallerProfile {
    data: Option<HashMap<String, Data>>,
    libraries: Option<Vec<Library>>,
    processors: Option<Vec<Processor>>,
    version_info: Option<VersionInfo>,
    install: Option<InstallMeta>,
}

#[derive(Deserialize)]
struct Arguments {
    game: Vec<String>,
    jvm: Option<Vec<String>>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct ForgeVersion {
    arguments: Option<Arguments>,
    minecraft_arguments: Option<String>,
    libraries: Vec<Library>,
    main_class: String,
}

#[derive(Deserialize)]
struct ForgeLibrary {
    name: String,
    #[serde(default = "default_repo")]
    url: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct InstallMeta {
    path: String,
    file_path: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct VersionInfo {
    minecraft_arguments: String,
    main_class: String,
    libraries: Vec<ForgeLibrary>,
}

fn default_repo() -> String {
    "https://libraries.minecraft.net/".to_string()
}

impl Manifest {
    pub async fn new(client: &Client) -> Result<Manifest> {
        read_json_url::<Manifest>(
            client,
            "https://files.minecraftforge.net/net/minecraftforge/forge/promotions_slim.json",
        )
        .await
    }

    /// Return the latest Forge version for a given Minecraft version
    pub fn latest(&self, id: &str) -> Option<Forge> {
        let search = format!("{}-latest", id);
        for (key, value) in &self.promos {
            if key == &search {
                return Some(Forge {
                    version: value.to_string(),
                    minecraft: id.to_string(),
                });
            }
        }
        None
    }
}

impl Forge {
    pub async fn setup(&self, client: &Client, version: &Version, runtime: &str) -> Result<()> {
        let root = Path::new("./libraries/net/minecraftforge").join(&self.version);
        let libraries = Path::new("./libraries");
        if !root.exists() {
            let installer = root.join("installer.jar");
            let url = format!("https://maven.minecraftforge.net/net/minecraftforge/forge/{0}-{1}/forge-{0}-{1}-installer.jar", self.minecraft, self.version);
            let mut size = download(client, url, &installer).await?;
            if size <= 200 {
                let url = format!("https://maven.minecraftforge.net/net/minecraftforge/forge/{0}-{1}-{0}/forge-{0}-{1}-{0}-installer.jar", self.minecraft, self.version);
                size = download(client, url, &installer).await?;
            }
            println!(
                "Downloaded Forge {}-{} ({} bytes)",
                self.minecraft, self.version, size
            );
            extract(&installer)?;
            remove_file(&installer)?;
            let maven = root.join("maven");
            if maven.exists() {
                recursive_dir_copy(&maven, libraries)?;
            }
            let installer = read_json_file::<InstallerProfile>(&root.join("install_profile.json"))?;
            if let Some(libs) = &installer.libraries {
                for lib in libs {
                    lib.download(client).await?;
                }
                if let Some(processors) = &installer.processors {
                    let mut map: HashMap<&str, &str> = HashMap::new();
                    let data = installer.data.expect("Valid processor data");
                    for (key, data) in &data {
                        map.insert(key.as_str(), data.client.as_str());
                    }
                    map.insert("SIDE", "client");
                    let jar = version.get_absolute_jar()?;
                    insert_path!(map, "MINECRAFT_JAR", jar);
                    for processor in processors {
                        if processor.is_applicable() {
                            processor.run(&root, libs, runtime, &map).await?;
                        }
                    }
                }
            }
            if let Some(install) = &installer.install {
                // Extract Forge from installer jar
                let local = root.join(&install.file_path);
                let file = get_maven_path(&install.path);
                let libs = libraries.join(file);
                let parent = libs.parent().expect("To have a parent dir");
                if !parent.exists() {
                    create_dir_all(parent)?;
                }
                copy(local, &libs)?;
            }
        }
        Ok(())
    }

    pub async fn build(&self, client: &Client, version: &mut Version) -> Result<()> {
        let root = Path::new("./libraries/net/minecraftforge").join(&self.version);
        let path = root.join("version.json");
        if !path.exists() {
            self.build_legacy(&root, client, version).await?;
            return Ok(());
        }
        let forge = read_json_file::<ForgeVersion>(&path)?;
        for lib in forge.libraries {
            let lib_name = lib.name.split(':').collect::<Vec<&str>>()[1];
            // Forge sometimes overrides libraries with newer versions
            version.libraries.retain(|l| {
                let name = l.name.split(':').collect::<Vec<&str>>()[1];
                name != lib_name
            });
            version.add_library(lib);
        }
        if let Some(args) = forge.arguments {
            for arg in args.game {
                version.add_game_arg(ArgumentEnum::String(arg));
            }
            if let Some(args) = &args.jvm {
                for arg in args.iter().rev() {
                    version.add_jvm_arg(ArgumentEnum::String(arg.to_string()));
                }
            }
        } else if let Some(args) = forge.minecraft_arguments {
            version.clear_game_args();
            for arg in args.split(' ') {
                version.add_game_arg(ArgumentEnum::String(arg.to_string()));
            }
        }
        version.set_main_class(forge.main_class);
        Ok(())
    }

    async fn build_legacy(
        &self,
        root: &Path,
        client: &Client,
        version: &mut Version,
    ) -> Result<()> {
        let installer = read_json_file::<InstallerProfile>(&root.join("install_profile.json"))?;
        if let Some(info) = &installer.version_info {
            for lib in &info.libraries {
                let library = if lib.name.starts_with("net.minecraftforge:forge:") {
                    // Use custom library with no checksum to allow local Forge library
                    let path = get_maven_path(&lib.name);
                    Library {
                        downloads: Downloads {
                            artifact: Some(Artifact {
                                path,
                                sha1: None,
                                url: "".to_string(),
                            }),
                            classifiers: None,
                        },
                        name: lib.name.to_string(),
                        rules: None,
                        natives: None,
                        skip_classpath: None,
                    }
                } else {
                    make_library(client, &lib.name, &lib.url, false).await?
                };
                version.add_library(library);
            }
            version.clear_game_args();
            for arg in info.minecraft_arguments.split(' ') {
                version.add_game_arg(ArgumentEnum::String(arg.to_string()));
            }
            version.set_main_class(info.main_class.to_string());
        } else {
            panic!("Expected version info in installer profile");
        }
        Ok(())
    }
}

impl Processor {
    pub fn is_applicable(&self) -> bool {
        let side = "client".to_string();
        match &self.sides {
            Some(sides) => sides.contains(&side),
            None => true,
        }
    }

    pub async fn run(
        &self,
        root: &Path,
        libs: &[Library],
        runtime: &str,
        map: &HashMap<&str, &str>,
    ) -> Result<()> {
        let jar = match libs.iter().find(|l| l.name == self.jar) {
            Some(x) => x,
            None => panic!("Unable to find jar for processor"),
        }
        .get_absolute_path()?;
        let main_class = get_main_class(&jar)?;
        let classpath = create_classpath(jar, libs, |l| self.classpath.contains(l))?;

        let mut command = Command::new(runtime);
        command.arg("-cp");
        command.arg(classpath);
        command.arg(main_class);
        for arg in &self.args {
            let result = replace_placeholders(arg, map);
            let transformed = transform(root, result.as_ref())?;
            command.arg(transformed.as_ref());
        }
        let process = command.spawn()?;
        let output = process.wait_with_output()?;
        if !output.status.success() {
            panic!("Forge processor failed to run!");
        }
        Ok(())
    }
}

fn get_main_class(path: &Path) -> Result<String> {
    let file = File::open(path)?;
    let mut archive = ZipArchive::new(file)?;
    let mut data = archive.by_name("META-INF/MANIFEST.MF")?;
    let manifest = read_to_string(&mut data)?;

    let prefix = "Main-Class: ";
    let main_class = manifest
        .lines()
        .find(|line| line.starts_with(prefix))
        .unwrap()
        .strip_prefix(prefix)
        .unwrap()
        .to_string();
    Ok(main_class)
}

fn transform<'a>(root: &Path, text: &'a str) -> Result<Cow<'a, str>> {
    if text.starts_with('[') && text.ends_with(']') {
        let dep = &text[1..text.len() - 1];
        let mut path = get_maven_path(dep);
        path.insert_str(0, "./libraries/");
        return Ok(Cow::from(path));
    } else if text.starts_with("/data/") {
        let path = root.join(&text[1..]);
        let str = canonicalize(path)?
            .to_str()
            .expect("A valid path")
            .to_string();
        return Ok(Cow::from(str));
    }
    Ok(Cow::Borrowed(text))
}
