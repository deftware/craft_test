use crate::{
    minecraft::library::{Artifact, Downloads, Library},
    util::Result,
};
use reqwest::Client;

pub async fn make_library(
    client: &Client,
    descriptor: &str,
    repo: &str,
    skip: bool,
) -> Result<Library> {
    let path = get_maven_path(descriptor);
    let url = format!("{repo}{path}");
    let sha_url = format!("{url}.sha1");
    let response = client.get(sha_url).send().await?;
    let sha1 = match response.status().is_success() {
        true => Some(response.text().await?.trim().to_lowercase()),
        false => None,
    };
    let artifact = Artifact { sha1, path, url };
    let library = Library {
        downloads: Downloads {
            artifact: Some(artifact),
            classifiers: None,
        },
        name: descriptor.to_string(),
        rules: None,
        natives: None,
        skip_classpath: Some(skip),
    };
    Ok(library)
}

/// Converts a maven descriptor to a path
pub fn get_maven_path(dependency: &str) -> String {
    let mut parts: Vec<&str> = dependency.split(':').collect();
    let mut ext = "jar";
    let last = parts.len() - 1;
    let idx = parts[last].find('@');
    if let Some(idx) = idx {
        ext = &parts[last][idx + 1..];
        parts[last] = &parts[last][0..idx];
    }
    let mut file = format!("{}-{}", parts[1], parts[2]);
    if parts.len() > 3 {
        file.push('-');
        file.push_str(parts[3]);
    }
    file.push('.');
    file.push_str(ext);
    format!(
        "{}/{}/{}/{file}",
        parts[0].replace('.', "/"),
        parts[1],
        parts[2]
    )
}
