use super::util::make_library;
use crate::minecraft::version::{ArgumentEnum, Version};
use crate::util::{read_json_url, Result};
use log::{debug, info};
use reqwest::Client;
use serde::Deserialize;
use std::fs::copy;
use std::path::{Path, PathBuf};

#[derive(Deserialize)]
struct Library {
    name: String,
    #[serde(default = "default_repo")]
    url: String,
}

#[derive(Deserialize)]
struct Arguments {
    game: Vec<String>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Aristois {
    libraries: Vec<Library>,
    main_class: String,
    minecraft_arguments: Option<String>,
    arguments: Option<Arguments>,
}

#[derive(Deserialize)]
pub struct Meta {
    pub id: String,
}

#[derive(Deserialize)]
pub struct Manifest {
    versions: Vec<Meta>,
}

fn default_repo() -> String {
    "https://libraries.minecraft.net/".to_string()
}

impl Manifest {
    pub async fn new(client: &Client, host: &Option<String>) -> Result<Manifest> {
        let host = match host {
            Some(host) => {
                info!("Using Aristois manifest {host}");
                host
            },
            None => "https://maven.aristois.net/manifest",
        };
        read_json_url::<Manifest>(client, host).await
    }

    pub fn get(&self, id: &str) -> Meta {
        Meta { id: id.to_string() }
    }

    pub fn list(&self) -> &[Meta] {
        &self.versions
    }
}

impl Meta {
    /// Creates a new instance of the Aristois version
    pub async fn create(
        &self,
        donor: bool,
        client: &Client,
        host: &Option<String>,
    ) -> Result<Aristois> {
        let host = match host {
            Some(host) => host,
            None => "https://maven.aristois.net/manifest",
        };
        let mut url = format!("{host}/{}.json", self.id);
        if donor {
            url.push_str("?donor=true");
        }
        read_json_url::<Aristois>(client, &url).await
    }
}

impl Aristois {
    pub async fn build(
        &self,
        client: &Client,
        version: &mut Version,
        jar: &Option<PathBuf>,
        emc: &Option<PathBuf>,
    ) -> Result<()> {
        debug!("Transforming Aristois libraries");
        for lib in &self.libraries {
            let is_skippable = matches!(
                lib.name.as_str(),
                "me.deftware:aristois:loader" | "me.deftware:aristois-d:loader"
            );
            if is_skippable && jar.is_some() {
                continue;
            }
            let mut library = make_library(client, &lib.name, &lib.url, false).await?;
            if lib.name.starts_with("me.deftware:EMC") {
                if let Some(emc) = emc {
                    if let Some(artifact) = &mut library.downloads.artifact {
                        artifact.sha1 = None;
                        copy(emc, PathBuf::from(format!("libraries/{}", &artifact.path)))?;
                    }
                }
            }
            version.add_library(library);
        }
        version.set_main_class(self.main_class.to_string());
        if let Some(args) = &self.arguments {
            for arg in &args.game {
                version.add_game_arg(ArgumentEnum::String(arg.to_string()));
            }
        }
        if let Some(args) = &self.minecraft_arguments {
            let vec = args.split(' ').collect::<Vec<&str>>();
            version.clear_game_args();
            for arg in vec {
                version.add_game_arg(ArgumentEnum::String(arg.to_string()));
            }
        }
        if let Some(jar_path) = jar {
            let root = PathBuf::from(format!("run/{0}/libraries/EMC/{0}", version.id));
            if !root.exists() {
                std::fs::create_dir_all(&root)?;
            }
            let path = root.join("Aristois.jar");
            copy(jar_path, path)?;
        }
        Ok(())
    }

    pub async fn setup_forge_deps(
        donor: bool,
        client: &Client,
        version: &mut Version,
        jar: &Option<PathBuf>,
        emc_jar: &Option<PathBuf>,
    ) -> Result<()> {
        // Aristois
        let aristois_jar = format!("../run/{0}/libraries/EMC/{0}/Aristois.jar", version.id);
        if let Some(jar_path) = jar {
            copy(jar_path, Path::new(&aristois_jar[1..]))?;
        } else {
            let loader = match donor {
                true => "me.deftware:aristois-d:latest",
                false => "me.deftware:aristois:latest",
            };
            let mut aristois =
                make_library(client, loader, "https://maven.aristois.net/", true).await?;
            if let Some(artifact) = &mut aristois.downloads.artifact {
                artifact.path = aristois_jar;
            }
            version.add_library(aristois);
        }
        // EMC
        let emc_path = format!("../run/{}/mods/EMC.jar", version.id);
        if let Some(path) = emc_jar {
            println!("Using custom Forge EMC jar {:?}", path);
            copy(path, Path::new(&emc_path[1..]))?;
        } else {
            let emc_version = format!("me.deftware:EMC-Forge:latest-{}", version.id);
            let mut emc = make_library(
                client,
                &emc_version,
                "https://maven.aristois.net/emc/",
                true,
            )
            .await?;
            if let Some(artifact) = &mut emc.downloads.artifact {
                artifact.path = emc_path;
            }
            version.add_library(emc);
        }
        Ok(())
    }
}
