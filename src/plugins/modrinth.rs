use std::collections::HashMap;
use std::{fs::remove_file, path::PathBuf};
use log::{info, warn};
use reqwest::Client;
use serde::Deserialize;

use crate::util::{download, file_checksum, read_json_url, Result};

const ENDPOINT: &str = "https://api.modrinth.com/v2";

#[derive(Deserialize)]
pub struct Hash {
    pub sha1: String
}

#[derive(Deserialize)]
pub struct Dependency {
    pub version_id: Option<String>,
    pub project_id: Option<String>,
    pub dependency_type: String
}

#[derive(Deserialize)]
pub struct File {
    pub url: String,
    pub filename: String,
    pub size: i32,
    pub primary: bool,
    pub hashes: Hash
}

#[derive(Deserialize)]
pub struct Version {
    pub game_versions: Vec<String>,
    pub loaders: Vec<String>,
    pub version_number: String,
    pub files: Vec<File>,
    pub version_type: String,
    pub dependencies: Vec<Dependency>
}

#[derive(Deserialize)]
pub struct Project {
    pub game_versions: Vec<String>,
    pub loaders: Vec<String>,
    pub title: String,
    pub slug: String,
    pub description: String
}

impl Version {
    pub async fn new(client: &Client, project: &str, slug: &str) -> Result<Self> {
        let url = format!("{ENDPOINT}/project/{project}/version/{slug}");
        read_json_url::<Self>(client, &url).await
    }
}

impl Project {
    pub async fn new(client: &Client, slug: &str) -> Result<Self> {
        let url = format!("{ENDPOINT}/project/{slug}");
        read_json_url::<Self>(client, &url).await
    }

    pub async fn get_versions(&self, client: &Client) -> Result<Vec<Version>> {
        let url = format!("{ENDPOINT}/project/{}/version", self.slug);
        read_json_url::<Vec<Version>>(client, &url).await
    }

    pub async fn latest_version(&self, client: &Client, loader: &str, game_version: &str) -> Result<Version> {
        let versions = self.get_versions(client).await?;
        for version in versions {
            if !version.game_versions.iter().any(|x| x == game_version) {
                continue
            }
            if !version.loaders.iter().any(|x| x == loader) {
                continue
            }
            // We have a compatible latest version
            return Ok(version)
        }
        Err(format!("Unable to find compatible version of {} for Minecraft {game_version} ({loader})", self.slug).into())
    }
}

pub struct ModResolver {
    /// A list of all filenames that were installed
    pub filenames: Vec<String>,
    pub ignored_projects: Vec<String>,
    pub visited_projects: HashMap<String, Version>,
    pub mods_dir: PathBuf,
    pub loader: String,
    pub game_version: String,
    pub client: Client,
    pub force_latest: bool
}

impl ModResolver {
    pub async fn run(&mut self, mod_slugs: &[String]) -> Result<()> {
        self.filenames.reserve(mod_slugs.len());
        self.visited_projects.reserve(mod_slugs.len());
        for slug in mod_slugs {
            let project = Project::new(&self.client, slug).await?;
            let latest_version = project.latest_version(&self.client, &self.loader, &self.game_version).await?;
            self.visit(&project, &project, latest_version, false).await?;
        }
        Ok(())
    }

    pub async fn visit(&mut self, parent: &Project, project: &Project, version: Version, ignore_deps: bool) -> Result<()> {
        // info!("Visiting {}", project.title);
        let visited_version = self.visited_projects.get(&project.slug);
        if let Some(visited_version) = visited_version {
            if visited_version.version_number != version.version_number {
                warn!("{} needs {} version {} but version {} was already visited", 
                    parent.slug, project.slug, version.version_number, visited_version.version_number);
            }
            return Ok(());
        }
        let mut is_modpack = false;
        for file in &version.files {
            if !file.primary {
                continue
            }
            if file.url.ends_with(".mrpack") {
                info!("Using modpack {} v{}", project.title, version.version_number);
                info!("\t{}", project.description);
                is_modpack = true;
                break
            }
            let full_path = self.mods_dir.join(&file.filename);
            self.filenames.push(file.filename.clone());
            if full_path.exists() {
                // Verify checksum
                let checksum = file_checksum(&full_path)?;
                if checksum == file.hashes.sha1 {
                    continue
                }
            }
            info!("Downloading {} version {} ({} bytes)", project.title, version.version_number, file.size);
            download(&self.client, &file.url, full_path).await?;
        }
        if ignore_deps {
            return Ok(())
        }
        let allowed_types = vec!["required", "embedded"];
        for dep in &version.dependencies {
            if !allowed_types.iter().any(|x| x == &dep.dependency_type) {
                continue
            }
            if let Some(project_id) = &dep.project_id {
                let dep_project = Project::new(&self.client, project_id).await?;
                if self.ignored_projects.contains(&dep_project.slug) {
                    continue
                }
                let version = if self.force_latest || dep.version_id.is_none() {
                    dep_project.latest_version(&self.client, &self.loader, &self.game_version).await?
                } else {
                    Version::new(&self.client, project_id, dep.version_id.as_ref().unwrap()).await?
                };
                Box::pin(self.visit(&project, &dep_project, version, ignore_deps || is_modpack)).await?;
            }
        }
        self.visited_projects.insert(project.slug.to_string(), version);
        Ok(())
    }    

    pub fn remove_non_installed(&self) -> Result<()> {
        for file in self.mods_dir.read_dir()? {
            let Ok(file) = file else {
                return Err("Unable to read file".into())
            };
            let Ok(name) = file.file_name().into_string() else {
                return Err("Unable to read file".into())
            };
            if name.starts_with("EMC") {
                continue
            }
            if !self.filenames.contains(&name) {
                warn!("Removing {name}");
                remove_file(file.path())?;
            }
        }
        Ok(())
    }
}
