use clap::Parser;
use craft_test::auth::{get_access_token, AuthFlow, AuthResponse};
use craft_test::minecraft::piston;
use craft_test::minecraft::version::ArgumentEnum;
use craft_test::plugins::aristois::{self, Aristois};
use craft_test::plugins::forge::Manifest as ForgeManifest;
use craft_test::plugins::modrinth::ModResolver;
use craft_test::plugins::util::make_library;
use craft_test::runtime::Runtime;
use craft_test::util::{recursive_dir_copy, Result};
use dunce::canonicalize;
use log::{debug, info};
use reqwest::Client;
use std::collections::HashMap;
use std::fs::{create_dir_all, remove_dir_all};
use std::io::{stdout, Write};
use std::path::{Path, PathBuf};
use std::process::Stdio;

#[derive(Parser)]
#[command(author, about, long_about = None)]
struct Args {
    /// Specifies the Minecraft version to run
    #[arg(short, long, default_value = "release")]
    version: String,

    /// Sign in with a Minecraft account
    /// using a provided application uuid
    #[arg(long, default_value = None)]
    auth: Option<String>,

    /// Generate an auth.aristois.net token
    #[arg(short, long, default_value_t = false)]
    gen_token: bool,

    /// Optional account name
    #[arg(long, default_value = None)]
    profile: Option<String>,

    /// Only download assets and libraries without running
    #[arg(short, long, default_value_t = false)]
    prepare: bool,

    /// Hide output from Minecraft
    #[arg(short, long, default_value = None)]
    quiet: bool,

    /// Remove the run directory after running
    #[arg(short, long, default_value = None)]
    clean: bool,

    /* Plugins */
    /// Run with Forge
    #[arg(short, long, default_value_t = false)]
    forge: bool,

    /* Aristois specific */
    /// Use a custom manifest server
    #[arg(long, default_value = None)]
    manifest: Option<String>,

    /// Run with Aristois
    #[arg(short, long, default_value_t = false)]
    aristois: bool,

    /// Run with the donor version of Aristois
    /// Requires an authenticated Minecraft account
    #[arg(short, long, default_value_t = false)]
    donor: bool,

    /// Use a custom EMC jar
    #[arg(long, default_value = None)]
    emc: Option<PathBuf>,

    /// Auto run Aristois tests
    #[arg(short, long, default_value_t = false)]
    test: bool,

    /// Use a custom Aristois jar file
    #[arg(short, long, default_value = None)]
    jar: Option<PathBuf>,

    /* Modrinth flags */
    /// Use a mod from Modrinth
    #[arg(short, long, num_args(0..), default_value = None)]
    r#mod: Option<Vec<String>>,

    /// Skip downloading Minecraft assets
    #[arg(long, default_value_t = false)]
    skip_assets: bool,

    /// Mods to ignore
    #[arg(long, num_args(0..), default_value = None)]
    ignore: Option<Vec<String>>,

    /// Remove mods not specified
    #[arg(long, default_value_t = false)]
    remove_unlisted: bool,

    /// Force the latest version of all mods
    #[arg(long, default_value_t = false)]
    force_latest: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"),
    );

    let version: &str = env!("CARGO_PKG_VERSION");
    info!("CraftTest version {version}");

    let args = Args::parse();
    let client = Client::new();

    let mut accounts_dir = PathBuf::from("./accounts");
    if let Some(profile) = &args.profile {
        accounts_dir.push(profile);
    }
    if !accounts_dir.exists() {
        create_dir_all(&accounts_dir)?;
    }

    let mut map = HashMap::new();
    let access_token = match &args.auth {
        Some(client_id) => get_access_token(client_id, &accounts_dir).await?,
        None => "0".to_string(),
    };
    let profile = AuthFlow::get_profile(&access_token, &client, &accounts_dir).await?;

    if args.gen_token && args.auth.is_some() {
        let response = AuthResponse::authenticate(&access_token, &profile, &client).await?;
        info!(
            "Your auth token is {}, and expires in {}",
            response.code, response.expires.text
        );
        info!(
            "Sign in by clicking https://aristois.net/login?code={}",
            response.code
        );
        return Ok(());
    }

    debug!("Loading manifests");
    let aristois_manifest = aristois::Manifest::new(&client, &args.manifest).await?;
    let minecraft_versions = piston::Manifest::new(&client).await?;

    let versions = match args.version.as_str() {
        "all" => {
            let list = aristois_manifest.list();
            let mut versions = Vec::with_capacity(list.len());
            for v in list {
                versions.push(&v.id);
            }
            versions
        }
        "release" => vec![&minecraft_versions.latest.release],
        "snapshot" => vec![&minecraft_versions.latest.snapshot],
        _ => vec![&args.version],
    };

    map.insert("user_type", "msa");
    map.insert("clientid", "0");
    map.insert("auth_xuid", "0");
    map.insert("auth_access_token", &access_token);
    map.insert("auth_session", &access_token);
    map.insert("auth_player_name", &profile.name);
    map.insert("auth_uuid", &profile.id);

    for v in versions {
        let Some(meta) = minecraft_versions.find(v) else {
            panic!("Unable to find Minecraft version {}", v)
        };
        let mut version = meta.create(&client).await?;
        let run_dir = version.get_run_dir()?;

        let runtime = Runtime::from(&version);
        let runtime_path = runtime.get_runtime(&client).await?;
        let canonicalized = canonicalize(&runtime_path)?;
        let java_binary = canonicalized.to_str().expect("To have a valid path");

        version.download_jar(&client).await?;
        if !args.skip_assets {
            version.download_assets(&client).await?;
        }

        if args.forge {
            let forge_manifest = ForgeManifest::new(&client).await?;
            let forge = forge_manifest.latest(&version.id).expect("A forge version");
            forge.setup(&client, &version, java_binary).await?;
            forge.build(&client, &mut version).await?;
            if args.aristois {
                Aristois::setup_forge_deps(args.donor, &client, &mut version, &args.jar, &args.emc)
                    .await?;
            }
        } else if args.aristois {
            let meta = aristois_manifest.get(v);
            let aristois = meta.create(args.donor, &client, &args.manifest).await?;
            aristois
                .build(&client, &mut version, &args.jar, &args.emc)
                .await?;
        }

        if cfg!(target_os = "macos") {
            // https://github.com/alphacep/vosk-api/issues/1198
            let repo = "https://repo1.maven.org/maven2/";
            let jna = make_library(&client, "net.java.dev.jna:jna:5.13.0", repo, false).await?;
            let jna_platform =
                make_library(&client, "net.java.dev.jna:jna-platform:5.13.0", repo, false).await?;
            version.libraries.retain(|l| !l.name.contains("jna"));
            version.libraries.push(jna);
            version.libraries.push(jna_platform);
        }
        version.download_libs(&client).await?;

        if args.aristois {
            if args.test {
                version.add_jvm_arg(ArgumentEnum::String("-Daristois.autoTest=true".into()));
            }
            version.add_jvm_arg(ArgumentEnum::String("-Daristois.enable=irc-mute".into()));
        }

        {
            let mods_dir = match args.forge {
                true => PathBuf::from(format!("run/{}/mods", version.id)),
                false => PathBuf::from(format!(
                    "libraries/me/deftware/EMC-F-v2/latest-{}",
                    version.id
                )),
            };
            let loader = match args.forge {
                true => "forge",
                false => "fabric",
            };
            let mut resolver = ModResolver {
                client: Client::new(),
                filenames: Vec::new(),
                force_latest: args.force_latest,
                game_version: version.id.to_string(),
                loader: loader.to_string(),
                ignored_projects: Vec::new(),
                mods_dir,
                visited_projects: HashMap::new(),
            };
            if let Some(ignored_projects) = &args.ignore {
                resolver
                    .ignored_projects
                    .extend_from_slice(ignored_projects);
            }
            if let Some(mods) = &args.r#mod {
                resolver.run(mods).await?;
            }
            if args.remove_unlisted {
                resolver.remove_non_installed()?;
            }
        }

        if !args.prepare {
            let mut cmd = version.get_command(java_binary, &run_dir, &map).await?;
            info!("Launching Minecraft {}", version.id);
            if args.quiet {
                cmd.stdout(Stdio::piped());
                cmd.stderr(Stdio::piped());
            }
            let process = cmd.spawn()?;
            let output = process.wait_with_output()?;
            if !output.status.success() {
                if args.quiet {
                    stdout()
                        .write_all(&output.stdout)
                        .expect("To have written stderr buffer to stdout");
                    stdout()
                        .write_all(&output.stderr)
                        .expect("To have written stderr buffer to stdout");
                }
                panic!("Minecraft {} has crashed!", version.id);
            }
            if args.aristois && args.test {
                info!("Aristois tests for Minecraft {} passed!", version.id);
                let results = Path::new("results");
                let output_dir = results.join(&version.id);
                // Copy screenshots
                let screenshots = run_dir.join("test-runs").join(&version.id);
                if !results.exists() {
                    std::fs::create_dir_all(results)?;
                }
                recursive_dir_copy(&screenshots, &output_dir)?;
                // Copy logs
                std::fs::copy(
                    run_dir.join("logs").join("latest.log"),
                    output_dir.join("latest.log"),
                )?;
            }
            if args.clean {
                remove_dir_all(&run_dir)?;
            }
        }
    }
    Ok(())
}
