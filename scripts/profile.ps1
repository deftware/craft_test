# Profile for running craft test from Powershell with ease

function Run-Minecraft {
    $AuthToken = "<token>" # Replace with a valid Azure application client id
    $CurrentDir = $PWD.Path

    if ($args.IndexOf('--profile') -eq -1) {
        # Write-Output "Using default profile"
        $args += "--profile"
        $args += "main"
    }

    # Custom Aristois jar
    $jarIndex = $args.IndexOf('--jar')
    if ($jarIndex -ne -1 -and ($jarIndex + 1) -lt $args.Count) {
        $JarFilePath = $args[$jarIndex + 1]
        if ($JarFilePath.StartsWith('.\')) {
            $JarFilePath = $JarFilePath.Substring(2)
        }
        $args[$jarIndex + 1] = Join-Path $CurrentDir $JarFilePath
    }

    # Custom EMC jar
    $EMCIndex = $args.IndexOf('--emc')
    if ($EMCIndex -ne -1 -and ($EMCIndex + 1) -lt $args.Count) {
        $JarFilePath = $args[$EMCIndex + 1]
        if ($JarFilePath.StartsWith('.\')) {
            $JarFilePath = $JarFilePath.Substring(2)
        }
        $args[$EMCIndex + 1] = Join-Path $CurrentDir $JarFilePath
    }

    Set-Location "<path>" # Replace with path to this repo on your disk
    try {
        # $env:RUST_LOG="debug"
        # cargo run -q -- --auth $AuthToken $args
        target\release\craft_test.exe --auth $AuthToken $args
    } catch {
        throw $_
    } finally {
        Set-Location $CurrentDir   
    }
}

# Usage: minecraft -v release [-ad]
Set-Alias minecraft Run-Minecraft
