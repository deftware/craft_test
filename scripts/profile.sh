#!/bin/bash

function run_minecraft {
    auth_token="<token>" # Replace with a valid Azure application client id
    args=("$@")
    # Find path supplied to argument
    for (( array_idx=1;  array_idx < ${#args[@]};  array_idx++ )); do
        arg=$args[$array_idx]
        if [[ $arg = "--jar" || $arg = "--emc" ]]; then
            index=$((array_idx + 1))
            path=$args[$index]
            if ! [[ $path = /* ]]; then
                args[$index]="$PWD/$path"
            fi
            continue
        fi
    done 
    # Append default profile if needed
    if ! [[ "$*" =~ --profile ]]; then 
        args+=(--profile)
        args+=(main)
    fi
    current_dir=$(pwd)
    trap "cd $current_dir" SIGINT
    cd "<path>" # Replace with path to this repo on your disk
    # cargo run -q -- --auth $auth_token ${args[@]}
    ./target/release/craft_test --auth $auth_token ${args[@]}
    cd $current_dir
}

# Usage: minecraft -v release [-ad]
alias minecraft=run_minecraft
